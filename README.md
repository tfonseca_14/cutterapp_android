<b>PROJECT TITLE:</b> Cutter App

<b>PURPOSE OF PROJECT:</b> Aplicación desarrollada especificamente para Bibliotecologos 
y/o estudiantes de bibliotecologia. Se utiliza un algoritmo de orden 
lexicografico para poder realizar la tarea.

<b>VERSION: 1.0.3</b>

<b>AUTHORS: </b>Tyron Fonseca Alvarado - tyron.alva1496@gmail.com


Este proyecto utiliza las siguientes librerias:

<b>Kotlin (Lenguaje de Programación)</b>
Copyright 2000–2018 JetBrains s.r.o.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use 
this file except in compliance with the License. You may obtain a copy of the 
License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed 
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
CONDITIONS OF ANY KIND, either express or implied. See the License for the 
specific language governing permissions and limitations under the License.
    

<b>OpenCSV 4.0 (Librería)</b>
Copyright 2017 Glen Smith and Scott Conway

Licensed under the Apache License, Version 2.0 (the "License"); you may not use 
this file except in compliance with the License. You may obtain a copy of the 
License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed 
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
CONDITIONS OF ANY KIND, either express or implied. See the License for the 
specific language governing permissions and limitations under the License.